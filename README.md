# Bencode library for C++
This is a simple library with two public functions: `encode` and `decode`.<br>
Tests are made with <a href="https://gitlab.com/arnovanliere/cpp-test">this test-framework</a>.

## Usage
1. Clone this repo `git clone git@gitlab.com:arnovanliere/bencode-cpp.git`
2. Generate Makefile `cmake -S . -B build`
3. Build library `cd build && make bencode`
3. Link the generated library (`build/libbencode.so`) with you executable.