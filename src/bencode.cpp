#include <iostream>
#include "bencode.h"
#include "bencode_exception.h"

std::string bencode::encode(const std::vector<bencode_val> &data) {
    std::string str;
    for (bencode_val d : data) str += encode_val(d);
    return str;
}

std::vector<bencode_val> bencode::decode(std::string data) {
    std::vector<bencode_val> decoded;
    decode(data, decoded, false);
    return decoded;
}

// Private
std::string bencode::encode_val(bencode_val data) {
    switch (data.type) {
        case bencode_type::INT:
            return encode_int(data.val.int_val);
        case bencode_type::BYTE_ARRAY:
            return encode_byte_array(data.val.byte_array);
        case bencode_type::LIST:
            return encode_list(data.val.list);
        case bencode_type::DICTIONARY:
            return encode_dictionary(data.val.dictionary);
        default:
            throw bencode_exception("invalid data type");
    }
}

std::string bencode::encode_int(int64_t data) {
    return "i" + std::to_string(data) + "e";
}

std::string bencode::encode_byte_array(std::vector<unsigned char> *data) {
    std::string str = bencode_val::to_string(data);
    return std::to_string(str.size()) + ":" + str;
}

std::string bencode::encode_list(std::vector<bencode_val> *data) {
    std::string str = "l";
    for (bencode_val d : *data) str += encode_val(d);
    return str + "e";
}

std::string bencode::encode_dictionary(std::map<std::string, bencode_val> *data) {
    std::string str = "d";
    for (const auto &val : *data) {
        str += std::to_string(val.first.length()) + ":" + val.first;
        str += encode_val(val.second);
    }
    return str + "e";
}

std::string bencode::decode(std::string &data, std::vector<bencode_val> &decoded, bool as_list) {
    bencode_val val{};

    if (data.empty()) {
        return data;
    }

    // If this is decoded as list, it should end with "e"
    if (as_list && data.empty()) {
        throw bencode_exception("list should end with e");
    } else if (as_list && data[0] == 'e') {
        return data;
    }

    if (data[0] == 'd') {
        // Parse dictionary
        val.type = bencode_type::DICTIONARY;
        val.val.dictionary = decode_dictionary(data);
    } else if (data[0] == 'i') {
        // Parse integer
        val.type = bencode_type::INT;
        val.val.int_val = decode_int(data);
    } else if (data[0] == 'l') {
        // Parse list
        val.type = bencode_type::LIST;
        val.val.list = decode_list(data);
    } else if (data[0] >= '0' && data[0] <= '9') {
        // Parse byte string
        val.type = bencode_type::BYTE_ARRAY;
        val.val.byte_array = decode_byte_array(data);
    } else {
        // Invalid character found
        throw bencode_exception("invalid character: " + std::to_string(data[0]));
    }

    // Push parsed value
    decoded.push_back(val);

    return decode(data, decoded, as_list);
}

int64_t bencode::decode_int(std::string &data) {
    std::string int_str;
    int i;

    // Start at 1 to skip the "i" in "data"
    for (i = 1; i < data.length(); i++) {
        // "e" indicates end of integer
        if (data[i] == 'e') {
            i++;
            break;
        }
        if ((data[i] >= '0' && data[i] <= '9') || (i == 1 && data[i] == '-')) {
            // Append integer to string
            int_str += data[i];
        } else {
            // Invalid character found
            throw bencode_exception("invalid character: " + std::to_string(data[i]));
        }
    }

    // Remove integer from the data
    data = data.erase(0, i);

    // Return result of integer conversion
    return std::stoi(int_str);

}

std::vector<unsigned char> *bencode::decode_byte_array(std::string &data) {
    std::string length_str;
    int i;

    // Read the length of the byte-string until ":"
    for (i = 0; i < data.size(); i++) {
        // ":" indicates the end of the length
        if (data[i] == ':') {
            i++;
            break;
        }
        if (data[i] >= '0' && data[i] <= '9') {
            // Append integer to string
            length_str += data[i];
        } else {
            // Invalid character found
            throw bencode_exception("invalid character: " + std::to_string(data[i]));
        }
    }

    // Convert string to integer
    int parsed_int = std::stoi(length_str);

    // Not enough of the data left to get a string of the specified length
    if (parsed_int > data.size() - i) {
        throw bencode_exception("byte-string length not valid");
    }

    // Extract byte-string
    std::string bytes = data.substr(length_str.size() + 1, length_str.size() + parsed_int - 1);

    // Remove byte-string from the data
    data = data.erase(0, length_str.size() + parsed_int + 1);

    return bencode_val::to_bytes(bytes);
}

std::vector<bencode_val> *bencode::decode_list(std::string &data) {
    auto *list = new std::vector<bencode_val>();

    // Decode list. Start at 1 to skip "l".
    data = data.erase(0, 1);
    std::string remaining = decode(data, *list, true);

    // Remove the "e" from the data
    data = remaining.erase(0, 1);

    return list;
}

std::map<std::string, bencode_val> *bencode::decode_dictionary(std::string &data) {
    auto dictionary = new std::map<std::string, bencode_val>();

    // Decode as list
    std::vector<bencode_val> *list = decode_list(data);

    // There should be as much keys as values
    if (list->size() % 2 != 0) {
        throw bencode_exception("more keys than values");
    }

    // Set key/values in dictionary
    for (int i = 0; i < list->size() - 1; i += 2) {
        auto key = list->at(i);
        auto value = list->at(i + 1);
        if (key.type != bencode_type::BYTE_ARRAY) {
            throw bencode_exception("key is not a valid byte-array");
        }
        dictionary->insert_or_assign(bencode_val::to_string(key.val.byte_array), value);
    }

    return dictionary;
}
