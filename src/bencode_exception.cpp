#include "bencode_exception.h"

bencode_exception::bencode_exception(const std::string &reason) {
    this->reason = reason;
}

const char *bencode_exception::what() const noexcept {
    return reason.c_str();
}
