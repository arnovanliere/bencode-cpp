#include "bencode_val.h"
#include <iostream>
#include <vector>
#include <sstream>

bencode_val bencode_val::int_val(int64_t data) {
    bencode_val b_val = {};
    b_val.type = bencode_type::INT;
    b_val.val.int_val = data;
    return b_val;
}

bencode_val bencode_val::byte_array(std::vector<unsigned char> data) {
    bencode_val b_val = {};
    b_val.type = bencode_type::BYTE_ARRAY;
    b_val.val.byte_array = &data;
    return b_val;
}

bencode_val bencode_val::list(std::vector<bencode_val> data) {
    bencode_val b_val = {};
    b_val.type = bencode_type::LIST;
    b_val.val.list = &data;
    return b_val;
}

bencode_val bencode_val::dictionary(std::map<std::string, bencode_val> data) {
    bencode_val b_val = {};
    b_val.type = bencode_type::DICTIONARY;
    b_val.val.dictionary = &data;
    return b_val;
}

std::ostream &operator<<(std::ostream &os, const bencode_type data) {
    switch (data) {
        case bencode_type::DICTIONARY: {
            os << "dictionary";
            break;
        }
        case bencode_type::BYTE_ARRAY: {
            os << "byte-array";
            break;
        }
        case bencode_type::LIST: {
            os << "list";
            break;
        }
        case bencode_type::INT: {
            os << "int";
        }
    }
    return os;
}

bool bencode_val::operator==(const bencode_val &other) const {
    bool same_type = this->type == other.type;
    if (!same_type) return false;
    switch (this->type) {
        case bencode_type::DICTIONARY:
            return *(this->val.dictionary) == *(other.val.dictionary);
        case bencode_type::INT:
            return this->val.int_val == other.val.int_val;
        case bencode_type::LIST:
            return *(this->val.list) == *(other.val.list);
        case bencode_type::BYTE_ARRAY:
            return *(this->val.byte_array) == *(other.val.byte_array);
    }
}

bool bencode_val::operator!=(const bencode_val &other) const {
    return !(*this == other);
}

std::string bencode_val::to_string(const bencode_val &data) {
    std::string str;
    std::stringstream ss;
    ss << "Type: " << data.type << std::endl << "Values: ";
    switch (data.type) {
        case bencode_type::DICTIONARY: {
            ss << data.val.dictionary << std::endl;
            break;
        }
        case bencode_type::BYTE_ARRAY: {
            ss << to_string(data.val.byte_array) << std::endl;
            break;
        }
        case bencode_type::LIST: {
            ss << to_string(data.val.list) << std::endl;
            break;
        }
        case bencode_type::INT: {
            ss << data.val.int_val << std::endl;
        }
    }
    return ss.str();
}

std::string bencode_val::to_string(std::vector<bencode_val> *data) {
    std::string str;
    for (auto d : *data) str += to_string(d);
    return str;
}

std::string bencode_val::to_string(std::vector<unsigned char> *data) {
    std::string str;
    for (auto c : *data) str += c;
    return str;
}

std::vector<unsigned char> *bencode_val::to_bytes(const std::string &data) {
    auto *bytes = new std::vector<unsigned char>();
    for (unsigned char c : data) bytes->push_back(c);
    return bytes;
}

