#ifndef BENCODE_VAL_H
#define BENCODE_VAL_H

#include <map>
#include <string>
#include <vector>

// Enum class to keep track which value is saved in the class
enum class bencode_type {
    INT,
    BYTE_ARRAY,
    LIST,
    DICTIONARY
};

// Parsed bencode value.
class bencode_val {
public:
    /**
     * Generate int value.
     * @param data The integer to set in the class.
     * @return     The instantiated class.
     */
    static bencode_val int_val(int64_t data);

    /**
     * Generate byte-array value.
     * @param data The byte-array to set in the class.
     * @return     The instantiated class.
     */
    static bencode_val byte_array(std::vector<unsigned char> data);

    /**
     * Generate list value.
     * @param data The list to set in the class.
     * @return     The instantiated class.
     */
    static bencode_val list(std::vector<bencode_val> data);

    /**
     * Generate dictionary value.
     * @param data The dictionary to set in the class.
     * @return     The instantiated class.
     */
    static bencode_val dictionary(std::map<std::string, bencode_val> data);

    /**
     * Overload equals-to operator.
     * @param other The other bencode_val to check with.
     * @return      Whether both are the same or not.
     */
    bool operator==(const bencode_val &other) const;

    /**
     * Overload not-equal-to operator.
     * @param other The other bencode_val to check with.
     * @return      Whether both are the same or not.
     */
    bool operator!=(const bencode_val &other) const;

    /**
     * Print bencode_val struct.
     * @param os   The output stream.
     * @param data The struct to write.
     * @return     The output stream.
     */
    static std::string to_string(const bencode_val &data);

    /**
     * Print vector met bencode_val structs.
     * @param os   The output stream.
     * @param data The vector to write.
     * @return     The output stream.
     */
    static std::string to_string(std::vector<bencode_val> *data);

    /**
     * Convert byte-array to string.
     * @param data The byte-array to convert.
     * @return     The string.
     */
    static std::string to_string(std::vector<unsigned char> *data);

    /**
     * Convert string to byte-array.
     * @param data The string to convert.
     * @return     The byte-array.
     */
    static std::vector<unsigned char> *to_bytes(const std::string &data);

    // The value
    union {
        int64_t int_val;
        std::vector<unsigned char> *byte_array;
        std::vector<bencode_val> *list;
        std::map<std::string, bencode_val> *dictionary;
    } val;

    // Which property contains the value
    bencode_type type;
};

#endif
