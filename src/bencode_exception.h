#ifndef BENCODE_EXCEPTION_H
#define BENCODE_EXCEPTION_H

#include <exception>
#include <string>

class bencode_exception : public std::exception {
public:
    /**
     * Default constructor.
     * @param reason The reason this exception is thrown.
     */
    explicit bencode_exception(const std::string &reason);

    /**
     * Retrieve the reason for this exception.
     * @return The reason.
     */
    [[nodiscard]] const char *what() const noexcept override;

private:
    // The reason this exception is thrown
    std::string reason;
};

#endif
