#ifndef BENCODE_H
#define BENCODE_H

#include "bencode_val.h"
#include <map>
#include <string>
#include <vector>

class bencode {
public:
    /**
     * Encode an array with bencode-values to string.
     * @param data The data to encode.
     * @return     The encoded data as string.
     */
    static std::string encode(const std::vector<bencode_val> &data);

    /**
     * Decode string according to bencode standard.
     * @param data The data to decode.
     * @return     The decoded data as vector with structs.
     */
    static std::vector<bencode_val> decode(std::string data);

private:
    /**
     * Encode a single value.
     * @param data The value to encode.
     * @return     The encoded value.
     */
    static std::string encode_val(bencode_val data);

    /**
     * Encode an integer.
     * @param data The integer to encode.
     * @return     The encoded integer.
     */
    static std::string encode_int(int64_t data);

    /**
     * Encode a byte-array.
     * @param data The byte-array to encode.
     * @return     The encoded byte-array.
     */
    static std::string encode_byte_array(std::vector<unsigned char> *data);

    /**
     * Encode a list.
     * @param data The list to encode.
     * @return     The encoded list.
     */
    static std::string encode_list(std::vector<bencode_val> *data);

    /**
     * Encode a dictionary.
     * @param data The dictionary to encode.
     * @return     The encoded dictionary.
     */
    static std::string encode_dictionary(std::map<std::string, bencode_val> *data);

    /**
     * Decode as string as bencode string.
     * @param data    The string to decode.
     * @param decoded The vector to save the values in.
     * @param as_list Whether to decode the string as list (has to end with "e").
     * @return        The remaining data to decode.
     */
    static std::string decode(std::string &data, std::vector<bencode_val> &decoded, bool as_list);

    /**
     * Decode the beginning of the string as integer.
     * @param data The string to decode.
     * @return     The parsed integer.
     */
    static int64_t decode_int(std::string &data);

    /**
     * Decode the beginning of the string as byte-array.
     * @param data The string to decode.
     * @return     The parsed byte-array.
     */
    static std::vector<unsigned char> *decode_byte_array(std::string &data);

    /**
     * Decode the beginning of the string as list.
     * @param data The string to decode.
     * @return     The parsed list.
     */
    static std::vector<bencode_val> *decode_list(std::string &data);

    /**
     * Decode the beginning of the string as dictionary.
     * @param data The string to decode.
     * @return     The parsed dictionary.
     */
    static std::map<std::string, bencode_val> *decode_dictionary(std::string &data);
};

#endif
