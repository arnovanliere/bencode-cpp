#include "../src/bencode.h"
#include "../inc/test.h"

int main() {
    auto t = new test();
    t->run("Decode list", [](test *t) {
        auto decoded = bencode::decode("l4:spami42ee");
        t->assert_eq(1ul, decoded.size());
        t->assert_eq(std::vector<bencode_val>{bencode_val::list({
                bencode_val::byte_array(std::vector<unsigned char>{'s', 'p', 'a', 'm'}),
                bencode_val::int_val(42)
        })}, decoded, bencode_val::to_string(&decoded));
    });
    t->run("Decode int", [](test *t) {
        auto decoded = bencode::decode("i42e");
        t->assert_eq(1ul, decoded.size());
        t->assert_eq(std::vector<bencode_val>{
            bencode_val::int_val(42)
        }, decoded, bencode_val::to_string(&decoded));
    });
    t->run("Decode negative int", [](test *t) {
        auto decoded = bencode::decode("i-42e");
        t->assert_eq(1ul, decoded.size());
        t->assert_eq(std::vector<bencode_val>{
            bencode_val::int_val(-42)
        }, decoded, bencode_val::to_string(&decoded));
    });
    t->run("Decode byte-string", [](test *t) {
        auto decoded = bencode::decode("4:spam");
        t->assert_eq(1ul, decoded.size());
        t->assert_eq(std::vector<bencode_val>{
                bencode_val::byte_array(std::vector<unsigned char>{'s', 'p', 'a', 'm'})
        }, decoded, bencode_val::to_string(&decoded));
    });
    t->run("Decode dictionary", [](test *t) {
        auto decoded = bencode::decode("d3:bar4:spame");
        t->assert_eq(1ul, decoded.size());
        t->assert_eq(std::vector<bencode_val>{
            bencode_val::dictionary(std::map<std::string, bencode_val>{
                {"bar", bencode_val::byte_array(std::vector<unsigned char>{'s', 'p', 'a', 'm'})}
            })
        }, decoded, bencode_val::to_string(&decoded));
    });
    t->run("Decode bencode string", [](test *t) {
        auto decoded = bencode::decode("l4:spami42eei42ei-42e4:spamd3:bar4:spame");
        t->assert_eq(5ul, decoded.size());
        t->assert_eq(std::vector<bencode_val>{
            bencode_val::list(std::vector<bencode_val>{
                bencode_val::byte_array(std::vector<unsigned char>{'s', 'p', 'a', 'm'}),
                bencode_val::int_val(42)
            }),
            bencode_val::int_val(42),
            bencode_val::int_val(-42),
            bencode_val::byte_array(std::vector<unsigned char>{'s', 'p', 'a', 'm'}),
            bencode_val::dictionary(std::map<std::string, bencode_val>{
                {"bar", bencode_val::byte_array(std::vector<unsigned char>{'s', 'p', 'a', 'm'})}
            })
        }, decoded, bencode_val::to_string(&decoded));
    });
    t->end();
}
