#include "../src/bencode.h"
#include "../inc/test.h"

int main() {
    auto t = new test();
    t->run("Encode list", [](test *t) {
        auto encoded = bencode::encode(std::vector<bencode_val>{
            bencode_val::list(std::vector<bencode_val>{
                bencode_val::byte_array(std::vector<unsigned char>{'s', 'p', 'a', 'm'}),
                bencode_val::int_val(42)
            })
        });
        t->assert_eq(std::string("l4:spami42ee"), encoded);
    });
    t->run("Encode int", [](test *t) {
        auto encoded = bencode::encode(std::vector<bencode_val>{
            bencode_val::int_val(42)
        });
        t->assert_eq(std::string("i42e"), encoded);
    });
    t->run("Encode negative int", [](test *t) {
        auto encoded = bencode::encode(std::vector<bencode_val>{
            bencode_val::int_val(-42)
        });
        t->assert_eq(std::string("i-42e"), encoded);
    });
    t->run("Encode dictionary", [](test *t) {
        auto encoded = bencode::encode(std::vector<bencode_val>{
                bencode_val::dictionary(std::map<std::string, bencode_val>{
                        {"bar", bencode_val::byte_array(std::vector<unsigned char>{'s', 'p', 'a', 'm'})}
                })
        });
        t->assert_eq(std::string("d3:bar4:spame"), encoded);
    });
    t->run("Encode bencode string", [](test *t) {
        auto encoded = bencode::encode(std::vector<bencode_val>{
            bencode_val::list(std::vector<bencode_val>{
                bencode_val::byte_array(std::vector<unsigned char>{'s', 'p', 'a', 'm'}),
                bencode_val::int_val(42)
            }),
            bencode_val::int_val(42),
            bencode_val::int_val(-42),
            bencode_val::byte_array(std::vector<unsigned char>{'s', 'p', 'a', 'm'}),
            bencode_val::dictionary(std::map<std::string, bencode_val>{
                {"bar", bencode_val::byte_array(std::vector<unsigned char>{'s', 'p', 'a', 'm'})}
            })
        });
        t->assert_eq(std::string("l4:spami42eei42ei-42e4:spamd3:bar4:spame"), encoded);
    });
    t->end();
}
